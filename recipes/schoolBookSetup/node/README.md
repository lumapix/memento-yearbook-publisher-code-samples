# Node.js school, book and team member setup example

## Requirements

You should have Node v7.6+ installed + npm or yarn.

## Run the script

`yarn` or `npm install`

`node index.js`
