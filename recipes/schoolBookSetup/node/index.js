const axios = require('axios');
const { get } = require('lodash');
const moment = require('moment');

// use the token generated from the publisher dashboard
const token = 'z2rnvHqIbBy4Xicfr2bZZARVzv3qrQXYXSsevdDw';
const client = axios.create({
	baseURL: 'https://yearbooks.me/api/external/publishers/v1',
	headers: {
		Authorization: `Bearer ${token}`,
		Accept: 'application/json',
	},
});

/**
 * Gets publisher book types.
 * @returns {Promise<any>}
 */
async function getBookTypes() {
	const response = await client.get(`book_types`);
	return response.data || response;
}

/**
 * Creates a new school.
 * @param inObjSchoolParams
 * @returns {Promise<any>}
 */
async function createSchool(inObjSchoolParams) {
	const response = await client.post(`school`, inObjSchoolParams);
	return response.data || response;
}

/**
 * Creates a new book for a school.
 * @param inSchoolId
 * @param inObjBookParams
 * @returns {Promise<any>}
 */
async function createBook(inSchoolId, inObjBookParams) {
	const response = await client.post(
		`school/${inSchoolId}/book`,
		inObjBookParams
	);
	return response.data || response;
}

/**
 * Creates a new team member for a book.
 * @param inStrBookId
 * @param inObjUserParams
 * @returns {Promise<any>}
 */
async function createTeamMember(inStrBookId, inObjUserParams) {
	const response = await client.post(
		`book/${inStrBookId}/team/user`,
		inObjUserParams
	);
	return response.data || response;
}

/**
 * Example of how to extract the cover / cover insert
 * pages from the book's form factor.
 *
 * @param inBookFormFactor
 * @returns {(*&{_pageTypeKey: *})[]}
 */
function getCoverPagesAndInserts(inBookFormFactor) {
	return Object.keys(inBookFormFactor.pageTypes).map((pageTypeKey) => {
		return {
			...inBookFormFactor.pageTypes[pageTypeKey],
			_pageTypeKey: pageTypeKey,
		};
	}).filter(page => page.pageType === 'cover' || page.pageType === 'coverInsert');
}

/**
 * This function demonstrates how to automate the setup of a school, book and team members.
 *
 * It can be adapted as necessary to your own in-house workflows.
 */
(async function () {
	// schoolID must be unique.
	// for testing, we use a random id.
	// for production use, remove this and
	// call the 'doesSchoolExist' API to check if the school ID is in use
	const outSchoolParams = {
		name: 'Publisher API Test School',
		schoolID: 'publisher_api_school_' + Math.floor(100000 + Math.random() * 900000),
	};
	// to create a book, we need to associate it with a book type, which provides
	// settings such as the form factor, page limit, etc.
	// in this example we are going to use the book type name to lookup the book type id
	// if you already have the id, this step can be skipped
	const bookTypeName = '8x8 Soft Hard Cover Demo Book';
	const bookTypes = await getBookTypes();
	console.log(`*** book types ***`);
	console.log(bookTypes);
	const bookType = bookTypes.data.find(
		(bookType) => get(bookType, 'attributes.name') === bookTypeName
	)
	if (!bookType) {
		throw new Error('book type id not found');
	}

	const bookTypeId = bookType.id;

	// The coverSettings param controls which cover pages and inserts are added to the book
	// In this example, we're going to add a soft and hard cover
	// To add all available covers / inserts / endsheets, simply omit the coverSettings param
	const coverSettings = [
		{
			pageTypeKey: 'softCover'
		},
		{
			pageTypeKey: 'hardCover'
		}
	]
	// we could also build the coverSettings object programmatically by filtering out the cover pages/inserts
	// that are present in the book's form factor
	// for more information on the schema, see: https://docs.yearbooks.me/api.html#create-book
	const coverPagesAndInserts = getCoverPagesAndInserts(bookType.attributes.form_factor)

	// for production use, set realistic book/cover deadlines
	const someFutureDate = moment().add(8, 'months').toISOString();
	// sample book params - update as necessary
	const outBookParams = {
		bookDueDate: someFutureDate,
		coverDueDate: someFutureDate,
		bookTypeId,
		bookName: 'Acme School Yearbook',
		coverSettings, // omit this parameter to setup the book with all available covers / inserts
		adminFirstName: 'John',
		adminLastName: 'Doe',
		adminEmail: 'johndoe@example.com',
	};
	// sample school admin params - update as necessary.
	// this user will be sent an invitation e-mail to accept
	// the Memento Yearbook terms of use, school COPPA regulations
	// and set an account password.
	const outUserParams = {
		first_name: 'Test',
		last_name: 'User',
		email: 'johndoe@example.com',
		password: 'password',
		role: 'editor_in_chief',
		username: 'someusername',
	};
	try {
		// here we are going to setup a school, book and add a new team member.
		// error control here is simplified for brevity. For production use, you'll want
		// to handle errors at each step.
		const school = await createSchool(outSchoolParams);
		console.log(`*** school created ***`);
		console.log(school);
		const book = await createBook(get(school, 'data.id'), outBookParams);
		console.log(`*** book created ***`);
		console.log(book);
		const teamUser = await createTeamMember(
			get(book, 'data.id'),
			outUserParams
		);
		console.log(`*** teamUser created ***`);
		console.log(teamUser);
	} catch (error) {
		const errorReport =
			get(error, 'response.data.meta') || get(error, 'response.data') || error;
		console.error(`*** error occurred ***`);
		console.log(errorReport);
		throw new Error(error);
	}
})();
