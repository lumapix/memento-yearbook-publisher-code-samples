<?php

/**
 * This is a example webhook endpoint in PHP which demonstrates how
 * to handle events sent from Memento Yearbook.
 *
 * No framework is used in this example.
 */

$payload = @file_get_contents('php://input');
$event = json_decode($payload);

// verify the request signature is valid
// update the signing secret with the value in your publisher dashboard
$secret = "whsec_Ge3tGXAEEeEGLDJqtMDGfmn9VoqJXtQz";
if (!isValidSignature($payload,getallheaders(),$secret)) {
    http_response_code(422);
    exit();
}

// this boolean property indicates this is a test event sent from the publisher dashboard
$isTestEvent = $event->is_test_event === true;
if ($isTestEvent) {
    // you may wish to simply acknowledge a test event and return early
    http_response_code(200);
    exit();
}

// Handle the event
switch ($event->type) {
    case 'proof.submitted':
        // this event is fired when a proof (book or cover) is ready to print
        // the payload contains a proofing object with information about the school and a link to the final pdf (zip)
        // proof->type will be either: 'book' or 'cover'
        $submittedProof = $event->data;
        // Then define and call a method to handle downloading the PDF
        // This code should run asynchronously outside of this webhook endpoint
        // handleProofDownload($submittedProof)
        break;
    case 'proof.cancelled':
        // this event is fired when a submitted proof (book or cover) is cancelled by the publisher
        // it can be used to void the yearbook submission internally
        // the proof->type will be either: 'book' or 'cover'
        $cancelledProof = $event->data;
        // Then define and call a method to handle cancelling this proof
        // This code should run asynchronously outside of this webhook endpoint
        // handleProofCancelled($cancelledProof)
        break;
    default:
        echo 'Unhandled event type ' . $event->type;
}

// Return a 200 response to acknowledge the receipt of the event as quickly as possible,
// since Memento Yearbook retries the event if a response is not
// sent within a reasonable time.

// As such, any long running code (e.g. downloading a PDF) in the event handlers above
// should be run asynchronously to prevent timeouts
http_response_code(200);

/**
 * Determines if the webhook signature of the request is valid.
 *
 * This allows you to verify that the events were sent by Memento Yearbook
 * and not a third party.
 *
 * @param $request
 * @param $headers
 * @param $secret
 * @return bool
 */
function isValidSignature($request, $headers, $secret) {
    $signature = null;

    if (isset($headers['Signature'])) {
        $signature = $headers['Signature'];
    }

    $computedSignature = hash_hmac('sha256', $request, $secret);

    return hash_equals($signature, $computedSignature);
}
